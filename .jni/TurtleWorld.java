import greenfoot.*;

/**
 * The TurtleWorld class defines a TurtleWorld of 11 cells on the X axis by 5 cells 
 * on the Y axis, where each cell is drawn using a 60x60 size image 
 * built from the "sand.jpg" file. When a scenario is executed, Greenfoot 
 * executes the act() method of all the actors repeatedly, that is, the 
 * execution of a Greenfoot scenario is a sequence of cycles and in each 
 * cycle the act () method of all the objects it contains is executed 
 * scenario.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class TurtleWorld extends World {

	public TurtleWorld() {
    	super(width(), height(), cellSize());
		start();
	}
    
    public void start(){
        start_();
    }
    private native void start_();

    public void act(){
        act_();
    }
    private native void act_();

    public Turtle getTurtle() {
        return getTurtle_();
    }
    private native Turtle getTurtle_();

    private native static int width();
    private native static int height();
    private native static int cellSize();
    
    /**
     * Count the actors number of a class added to the world
     * 
     * @param cls is the class of actor
     * @return the actors number of a class added to the world
     */
    public int actorsNumber(Class<? extends Actor> cls) {
        return getObjects(cls).size();
    }

    /**
     * Count the actors number of a class added to the world at x,y cell
     * 
     * @param cls is the class of actor
     * @return the actors number of a class added to the world at x,y cell
     */
    public int actorsNumberAt(int x, int y, Class<? extends Actor> cls) {
        return getObjectsAt(x, y, cls).size();
    }

    /**
     * Find the oldest actor of a class added to the world
     * 
     * @param cls is the class of actor
     * @return the oldest actor or null when there is not any actor of cls class
     */
    public <T extends Actor> T oldestActor(Class<T> cls) {
        return oldestActor(cls, 0);
    }

    /**
     * Find the nth oldest actor of a class added to the world
     * 
     * @param cls is the class of actor
     * @param nth is the order where it was added
     * @return the nth oldest actor or null when there 
     *         is not any nth oldest actor of cls class
     */
    public <T extends Actor> T oldestActor(Class<T> cls, int nth) {
        if (getObjects(cls).size() > nth) {
            return getObjects(cls).get(nth);
        } else {
            return null;
        }
    }

    /**
     * Find the oldest actor of a class added to the world at x,y cell
     * 
     * @param x X-coordinate of the cell to be checked.
     * @param y Y-coordinate of the cell to be checked.
     * @param cls is the class of actor
     * @return the oldest actor at x,y cell or null when there is  
     *         not any actor of cls class at x,y cell
     */
    public <T extends Actor> T oldestActorAt(int x, int y, Class<T> cls) {
        return oldestActorAt(x, y, cls, 0);
    }

    /**
     * Find the nth oldest actor of a class added to the world at x,y cell
     * 
     * @param x X-coordinate of the cell to be checked.
     * @param y Y-coordinate of the cell to be checked.
     * @param cls is the class of actor
     * @param nth is the order where it was added
     * @return the nth oldest actor at x,y cell or null when there  
     *         is not any nth oldest actor of cls class at x,y cell
     */
    public <T extends Actor> T oldestActorAt(int x, int y, Class<T> cls, int nth) {
        if (getObjectsAt(x, y, cls).size() > nth) {
            return getObjectsAt(x, y, cls).get(nth);
        } else {
            return null;
        }
    }

    static {
        System.load(new java.io.File(".jni", "TurtleWorld_jni.so").getAbsolutePath());
    }
}
