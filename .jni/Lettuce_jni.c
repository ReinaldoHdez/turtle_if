/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "Lettuce.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_Lettuce_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startLettuce(object);
}

JNIEXPORT jobject JNICALL Java_Lettuce_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actLettuce(object);
}

JNIEXPORT jstring JNICALL Java_Lettuce_getLocation_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    return toJstring(getLocationLettuce(object));
}
