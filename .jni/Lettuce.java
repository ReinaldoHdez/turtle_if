import greenfoot.*;  

/**
 * The Lettuce class defines a game actor that has a 50x40 size image built 
 * from the "Lettuce.png" file. The act() method of an object of the Lettuce 
 * class defines the behavior of an object of this class.The act() method 
 * declared in the Lettuce defines the Lettuce behaviour in each cycle of the 
 * scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class Lettuce extends Actor {
    
	public Lettuce() {
		start();
	}

    public void start(){
        start_();
    }
    private native void start_();
    
    public void act(){
        act_();
    }
    private native void act_();
    
    public String getLocation(){
        return getLocation_();
    }
    private native String getLocation_();
    
    static {
        System.load(new java.io.File(".jni", "Lettuce_jni.so").getAbsolutePath());
    }
}
