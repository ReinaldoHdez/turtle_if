import greenfoot.*;  

/**
 * The Turtle class defines a game actor that has a 50x40 size image built 
 * from the "turtle.png" file. The act() method of an object of the Turtle 
 * class defines the behavior of an object of this class.The act() method 
 * declared in the Turtle defines the turtle behaviour in each cycle of the 
 * scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class Turtle extends Actor {
    
	public Turtle() {
		start();
	}

    public void start(){
        start_();
    }
    private native void start_();
    
    public void act(){
        act_();
    }
    private native void act_();
    
    public String getLocation(){
        return getLocation_();
    }
    private native String getLocation_();
    
    static {
        System.load(new java.io.File(".jni", "Turtle_jni.so").getAbsolutePath());
    }
}
