/**
 * Tests of Turtle. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "Lettuce.c"

void testGetLocation() {
    // Given
    Actor lettuce = newActor("Lettuce");
    // When
    const char* location = getLocationLettuce(lettuce);
    // Then
    assertEquals_String("out of the world",location);
}

void testGetLocationAfterAdd_3_1()  {
    // Given
    Actor lettuce = newActor("Lettuce");
    World turtleWorld = newWorld("TurtleWorld");
    addActorToWorld(turtleWorld, lettuce, 3, 1);
    // When
    const char* location = getLocationLettuce(lettuce);
    // Then
    assertEquals_String("3,1", location);
}
