/**
 * Tests of Turtle. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "TurtleWorld.c"
#include "Turtle.c"

void testGetLocation() {
    // Given
    Actor turtle = newActor("Turtle");
    // When
    const char* location = getLocationTurtle(turtle);
    // Then
	assertEquals_String("out of the world", location);
}

void testGetLocationAfterAdd_1_3()  {
    // Given
    World turtleWorld = newWorld("TurtleWorld");
    Actor turtle = newActor("Turtle");
    addActorToWorld(turtleWorld, turtle, 1, 3);
    // When
    const char* location = getLocationTurtle(turtle);
    // Then
	assertEquals_String("1,3", location);
}

void testTurtleAct() {
    // Given
    World turtleWorld = newWorld("turtleWorld");
    Actor turtle = getTurtle(turtleWorld);
    
    // When
    actTurtle(turtle);
    // Then
    assertEquals_String("6,2", getLocationTurtle(turtle));
}

void testTurtleActTwice() {
    // Given
    World turtleWorld = newWorld("turtleWorld");
    Actor turtle = getTurtle(turtleWorld);
    
    // When
    actTurtle(turtle);
    actTurtle(turtle);
    
    // Then
    assertEquals_String("7,2", getLocationTurtle(turtle));
}

void testTurtleActAfterMouseClicked() {
    // Given
    World turtleWorld = newWorld("TurtleWorld");
    Actor turtle = getTurtle(turtleWorld);
    
    // When
    clickMouse(5,2);
    actTurtle(turtle);
    // Then
    assertEquals_String("6,3", getLocationTurtle(turtle));
}

void testTurtleActAfterMouseClickedTwice() {
      // Given
    World turtleWorld = newWorld("TurtleWorld");
    Actor turtle = getTurtle(turtleWorld);
    
    // When
    clickMouse(5,2);
    actTurtle(turtle);
    clickmouse(6,3);
    actTurtle(turtle);
    
    // Then
    assertEquals_String("6,4", getLocationTurtle(turtle));
}

void testLettuceIsNotInTurtleWorldBecauseItWasEaten() {
    // Given
    World turtleWorld = newWorld("TurtleWorld");
    Actor turtle = getTurtle(turtleWorld);
    Actor lettuce = newActor("Lettuce");
    addActorToWorld(turtleWorld, lettuce, 6, 2);
    
    // When
    actTurtle(turtle);
    // Then
	assertEquals_String("out of the world", getLocationTurtle(lettuce));
}

void testLettucesNumberIsZeroBecauseItWasEaten() {
    // Given
    World turtleWorld = newWorld("TurtleWorld");
    Actor turtle = getTurtle(turtleWorld);
    Actor lettuce = newActor("Lettuce");
    addActorToWorld(turtleWorld, lettuce, 6, 2);
    
    // When
    actTurtle(turtle);

    // Then
	assertEquals_int(0, getActorsNumber(turtleWorld, "Lettuce"));
}

