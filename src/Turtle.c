/**
 * The Turtle file defines a game actor that has a 50x40 size image built 
 * from the "Turtle.png" file. The act() method declared in the Turtle file 
 * defines the turtle behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its two images.
 */
void startTurtle(Actor turtle) {
	setImageFile(turtle, "Turtle.png");
	setImageScale(turtle, 50, 40);
	setGlobalInt(turtle, "distance", 1);
	setGlobalInt(turtle, "angle", 45);
}

void actTurtle(Actor turtle) {
    if(mouseClicked(turtle)) {
    	turn(turtle,getGlobalInt(turtle, "angle"));;
    }
    move(turtle,getGlobalInt(turtle, "distance"));
    removeTouching(turtle, "Lettuce");

}
    
const char* getLocationTurtle(Actor turtle) {
    if (getWorld(turtle) == NULL)  {
        return "out of the world";
    }
    if (getX(turtle) == 1 && getY(turtle) == 3) {
        return "1,3";
    }
     if(getX(turtle) == 7 && getY(turtle) == 2) {
        return "7,2";
    }
     if(getX(turtle) == 5 && getY(turtle) == 2) {
             return "5,2";
    }
     if(getX(turtle) == 6 && getY(turtle) == 2) {
        return "6,2";
    }
     if(getX(turtle) == 6 && getY(turtle) ==3) {
        return "6,3";
    }
    if(getX(turtle) == 6 && getY(turtle) == 4) {
        return "6,4";
    }
    if(getX(turtle) == 7 && getY(turtle) == 3) {
            return "7,3";
    }
    return NULL;
}
