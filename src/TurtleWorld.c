/**
 * The Board class defines a board of 11 cells on the X axis by 5 cells 
 * on the Y axis, where each cell is drawn using a 60x60 size image 
 * built from the "sand.jpg" file. When a scenario is executed, Greenfoot 
 * executes the act() method of all the actors repeatedly, that is, the 
 * execution of a Greenfoot scenario is a sequence of cycles and in each 
 * cycle the act () method of all the acts it contains is executed scenario.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

#define _width    11
#define _height   5
#define _cellSize 60

void startTurtleWorld(World turtleWorld) {
    setBackgroundFile(turtleWorld, "sand.jpg");
    Actor turtle = newActor("Turtle");
    addActorToWorld(turtleWorld, turtle, 5, 2);
    setGlobalActor(turtleWorld,"turtle", turtle);
}

Actor getTurtle(World turtleWorld) {
    return getGlobalActor(turtleWorld,"turtle");
}

void actTurtleWorld(World turtleWorld) {
    if (getActorsNumber(turtleWorld, "Lettuce") == 0) {
        addActorToWorld(turtleWorld, newActor("Lettuce"), 0  ,1);
        addActorToWorld(turtleWorld, newActor("Lettuce"), 1, 4);
        addActorToWorld(turtleWorld, newActor("Lettuce"), 7, 3);
        addActorToWorld(turtleWorld, newActor("Lettuce"), 8, 0);
        addActorToWorld(turtleWorld, newActor("Lettuce"), 9, 4);
        addActorToWorld(turtleWorld, newActor("Lettuce"), 6, 2);
}
}
