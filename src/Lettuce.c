/**
 * The Lettuce file defines a game actor that has a 40x40 size image built 
 * from the "Lettuce.png" file. The act() method declared in the Lettuce file 
 * defines the lettuce behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its two images.
 */
void startLettuce(Actor lettuce) {
	setImageFile(lettuce, "Lettuce.png");
	setImageScale(lettuce, 40, 40);
}

void actLettuce(Actor lettuce) {

}
    
const char* getLocationLettuce(Actor lettuce) {
    if (getWorld(lettuce) == NULL) {
        return "out of the world";
    }
    if (getX(lettuce) == 0 && getY(lettuce) == 1) {
                return "0,1";
    }
    if (getX(lettuce) == 1 && getY(lettuce) == 4) {
                return "1,4";
    }
    if (getX(lettuce) == 3 && getY(lettuce) == 1) {
            return "3,1";
    }
    if (getX(lettuce) == 7 && getY(lettuce) == 3) {
                return "7,3";
    }
    if (getX(lettuce) == 8 && getY(lettuce) == 0) {
                return "8,0";
    }
    if (getX(lettuce) == 9 && getY(lettuce) == 4) {
                return "9,4";
    }
    return NULL;
}
